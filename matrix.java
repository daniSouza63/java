import java.io.File;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Matrix {

	String filePath; // Directory for the archive Matrix .txt
	Scanner mySc = new Scanner(System.in); // Initialized Scanner to collect input from the user
	int matrixA[][] = null; // Array 2D for the first Matrix
	int matrixB[][] = null; // Array 2D for the second Matrix
	int numbRowsA = 0, numbColsA = 0; // Variables to collect sizes of Rows and Columns
	int numbRowsB = 0, numbColsB = 0;
	String line = "", line2 = ""; // random string to read "lines" from the archive
	int symbol; // variable identifying the option that user wants to operate. (Addition,
				// Subtraction, Multiplication, etc)
	int resultA[][] = null; // Array 2D (integer) to store the results of the operations for the first
							// matrix
	int resultB[][] = null; // Array 2D (integer) to store the results of the operations for the
													// second matrix
	Double resultDivA[][] = null; // Array 2D (Double) to store the results of the operations that involves
									// divisions (for the first matrix)
	Double resultDivB[][] = null; // Array 2D (Double) to store the results of the operations that involves
									// divisions (for the second matrix)
	boolean valid = true;
	int[] spliterInt = null;

	public Matrix() {
		// Main constructor,  here we set the matrix up, reading it from the file, respecting the file sample rules (first line is index.. and so on)

		filePath = chooseFileMatrix("Which file would you like to use?");

		try {
			Scanner myRd = new Scanner(new File(filePath)); // Calling the Scanner to the files text (Matrix.txt,
															// Matrix2.txt, Invalid.txt, Invalid2.txt)

			numbRowsA = Integer.parseInt(myRd.next()); // variable for size of rows in the matrix, it gets the char,
														// casting it to integer
			numbColsA = Integer.parseInt(myRd.next()); // variable for size of rows in the matrix, it gets the char,
														// casting it to integer

			matrixA = new int[numbRowsA][numbColsA]; // initialization of he first matrix

			myRd.close();
		} catch (Exception e) {
			System.out.println("The system can not find the right file");
			processRestartingProgram("Would you like to do another operation?");
		}

		try {
			Scanner myRd = new Scanner(new File(filePath));
			line = myRd.nextLine();

			for (int i = 0; i < numbRowsA; i++) {
				String spliter[] = myRd.nextLine().split(" ");
				if (spliter.length != numbColsA) {
					// the matrix can't be read if there's a gap, or empty value. sorry but we can't go on with this invalid file.
					System.out.println(
							"The number of items in the line is different of the index of the matrix. Is expected a "
									+ numbRowsA + "x" + numbColsA
									+ " matrix. \n It's not possible to continue the operation. \n  Please try another file.");
					processRestartingProgram("Would you like to do another operation?");
				}else {
					
					for (int s = 0; s < spliter.length; s++) {
						spliterInt = new int [spliter.length];
						
						try {
							// again here is another treatment to validate whether the matrix has only numbers or not and if not, the operation will be discarded 
							spliterInt[s] = Integer.parseInt(spliter[s]);
						}catch (Exception e) {
							System.out.println("There's an error. \n We can not find a way to convert the position '" + spliter[s] + "' to an Integer Value \n  It's not possible to continue the operation. \n   Please try another file.");
							processRestartingProgram("Would you like to do another operation?");
						}
					}
					
					for (int j = 0; j < numbColsA; j++) {
								
						matrixA[i][j] = Integer.parseInt(spliter[j]);
					}
				}
			}
		
			System.out.println("Matrices: \n"); 

			System.out.println(" Matrix A"); 
			// simple text print first matrix
			for (int i = 0; i < matrixA.length; i++) {
				for (int j = 0; j < matrixA[i].length; j++) {
					System.out.print(matrixA[i][j] + "\t");
				}
				System.out.println();
			}

			//checking the at in the file txt
			String at = myRd.nextLine();
			while (!at.equals("@")) {
				myRd.nextLine();
			}
			System.out.println("\n" + at);


			//preparing index of the second matrix
			String spliterIndex[] = myRd.nextLine().split(" ");
			numbRowsB = Integer.parseInt(spliterIndex[0]);
			numbColsB = Integer.parseInt(spliterIndex[1]);
			matrixB = new int[numbRowsB][numbColsB];


			for (int i = 0; i < numbRowsB; i++) {
				String spliter[] = myRd.nextLine().split(" ");
				for (int j = 0; j < numbColsB; j++)
					matrixB[i][j] = Integer.parseInt(spliter[j]);
			}
			//setting up the array in the code above
			System.out.println("\n" + " Matrix B ");
			//and printing it below, 
			for (int i = 0; i < matrixB.length; i++) {
				for (int j = 0; j < matrixB[i].length; j++) {
					System.out.print(matrixB[i][j] + "\t");
				}
				System.out.println();
			}
			myRd.close();
		} catch (Exception e) {
			System.out.println("file note found2");
		}

		 symbol = askUserOper("Choose a maths operation: (Please, use only numbers)");
		 /*here is been colected the user option from the menu
		 and delegate the option to the right operation,*/
		 delegUserOper(symbol);

	}

	private void saveResults3(String string, Double[][] resultDivA2, Double[][] resultDivB2) {
		// method to save the results in a file. This is specifically for double matrices 
		
		String resultFilePatch = "answer.txt";
		
		System.out.println("\n" + string);
		
		System.out.println("[1] - Yes");
		System.out.println("[2] - No");

		do { //using loop to validate the user input,  only numbers and just numbers are accepted.
			try {
				symbol = Integer.parseInt(mySc.nextLine());
				if (symbol < 1 || symbol > 2) {
					// not good
					System.out.println("Sorry, your number was not reconigized try it again (number: 1 or 2)");
					valid = false;
				} else {
					valid = true;
				}
			} catch (Exception e) {
				System.out.println("The option must be a number");
				valid = false;
			}
		} while (!valid);

		if (symbol == 1) {
		
			try (FileWriter fw = new FileWriter(resultFilePatch, true)){
			//I started the File Writer and used the same simple loop for write both matrices in that file. 
			
				fw.write(resultDivA.length + " " + resultDivA[0].length + "\n");
			
				for (int i = 0; i < resultDivA.length; i++) {
					for (int j = 0; j < resultDivA[i].length; j++) {
						fw.write(Math.round(resultDivA[i][j] * 1000) / 1000D + " ");
					}
					fw.write("\n");
				}
			
				fw.write("@ \n");
			
				fw.write(resultDivB.length + " " + resultDivB[0].length + "\n");
			
				for (int i = 0; i < resultDivB.length; i++) {
					for (int j = 0; j < resultDivB[i].length; j++) {
						fw.write(Math.round(resultDivB[i][j] * 1000) / 1000D + " ");
					}
					fw.write("\n");
				}
				
				fw.write("@ \n");
				fw.close();
				
				System.out.println("Your data has been saved in the file '" + resultFilePatch + "'");

				processRestartingProgram("Would you like to do another operation?");
					
			}catch (Exception e) {
				System.out.println("Error to write in the file");
			
				processRestartingProgram("Would you like to do another operation?");
			}
		
		} else {
			processRestartingProgram("Would you like to do another operation?");

		}

	}

	private void saveResults2(String string, int[][] resultA2, int[][] resultB2) {
		// method to save the results in a file. This is for two matrices at the same time
		
		String resultFilePatch = "answer.txt";
		
		System.out.println("\n" + string);
		
		System.out.println("[1] - Yes");
		System.out.println("[2] - No");

		do {	//using loop to validate the user input,  only numbers and just numbers are accepted.
			try {
				symbol = Integer.parseInt(mySc.nextLine());
				if (symbol < 1 || symbol > 2) {
					// not good
					System.out.println("Sorry, your number was not reconigized try it again (number: 1 or 2)");
					valid = false;
				} else {
					valid = true;
				}
			} catch (Exception e) {
				System.out.println("The option must be a number");
				valid = false;
			}
		} while (!valid);

		if (symbol == 1) {
		
			try (FileWriter fw = new FileWriter(resultFilePatch, true)){
			//I started the File Writer and I'm using the same simple loop for write both matrices in that file. 
			
				fw.write(resultA.length + " " + resultA[0].length + "\n");
			
				for (int i = 0; i < resultA.length; i++) {
					for (int j = 0; j < resultA[i].length; j++) {
						fw.write(resultA[i][j] + " ");
					}
					fw.write("\n");
				}
			
				fw.write("@ \n");
			
			
				fw.write(resultB.length + " " + resultB[0].length + "\n");
			
				for (int i = 0; i < resultB.length; i++) {
					for (int j = 0; j < resultB[i].length; j++) {
						fw.write(resultB[i][j] + " ");
					}
					fw.write("\n");
				}
				fw.write("@ \n");
				fw.close();
				
				System.out.println("Your data has been saved in the file '" + resultFilePatch + "'");

				processRestartingProgram("Would you like to do another operation?");
					
			}catch (Exception e) {
				System.out.println("Error to write in the file");
			
				processRestartingProgram("Would you like to do another operation?");
			}
		
		} else {
			processRestartingProgram("Would you like to do another operation?");

		}
		
		
	}
	
	private void saveResults(String string, int[][] resultA2) {
		// method to save the results in the file. This is for one matrix results only.
		
		String resultFilePatch = "answer.txt";
		
		System.out.println("\n" + string);
		
		System.out.println("[1] - Yes");
		System.out.println("[2] - No");

		do {	//using loop to validate the user input,  only numbers and just numbers are accepted.
			try {
				symbol = Integer.parseInt(mySc.nextLine());
				if (symbol < 1 || symbol > 2) {
					// not good
					System.out.println("Sorry, your number was not reconigized try it again (number: 1 or 2)");
					valid = false;
				} else {
					valid = true;
				}
			} catch (Exception e) {
				System.out.println("The option must be a number");
				valid = false;
			}
		} while (!valid);

		if (symbol == 1) {
			
			try (FileWriter fw = new FileWriter(resultFilePatch, true)){
		//same loop for from other saveResults method, appending to the file and writing one matrix only
			fw.write(resultA.length + " " + resultA[0].length + "\n");
			
			for (int i = 0; i < resultA.length; i++) {
				for (int j = 0; j < resultA[i].length; j++) {
					fw.write(resultA[i][j] + " ");
				}
				fw.write("\n");
			}
			
			fw.write("@ \n");
			
			fw.close();
			
			System.out.println("Your data has been saved in the file '" + resultFilePatch + "'");
			processRestartingProgram("Would you like to do another operation?");
						
		}catch (Exception e) {
			System.out.println("Error to write in the file");
			//in case of Exception, the program will do rolling back to another operation 
			processRestartingProgram("Would you like to do another operation?");
		}
			
		} else {
			processRestartingProgram("Would you like to do another operation?");

		}
		
		
		
	}

	private String chooseFileMatrix(String string) {
		// method to choose a file, the user can choose any of the 4 matrix files or can manually type a matrix file name. (don't forget to put the extension ".txt") 
		System.out.println();
		System.out.println(string);
		System.out.println("[1] - matrix.txt");
		System.out.println("[2] - matrix2.txt");
		System.out.println("[3] - invalidFile.txt");
		System.out.println("[4] - invalidFile2.txt");
		System.out.println("[5] - Enter manual");

		//a small loop to ensure that the user is using the menu above
		do {
			try {
				symbol = Integer.parseInt(mySc.nextLine());
				if (symbol < 0 || symbol > 5) {
					// not good
					System.out.println("Sorry, your number was not recognized try it again (a number between 1 and 5)");
					valid = false;
				} else {
					valid = true;
				}
			} catch (Exception e) {
				System.out.println("The option must be a number");
				valid = false;
			}
		} while (!valid);

		if (symbol == 1) {
			filePath = "matrix.txt";
		} else if (symbol == 2) {
			filePath = "matrix2.txt";
		} else if (symbol == 3) {
			filePath = "invalidFile.txt";
		} else if (symbol == 4) {
			filePath = "invalidFile2.txt";
		}else {
			System.out.println("You can type the file name desired just be careful about the extesion of the file.");
			filePath = mySc.nextLine();
			//filePath = "matrix3.txt";
		}

		return filePath;
	}

	private void processRestartingProgram(String string) {
		// a small method to restart the program

		System.out.println();
		System.out.println(string);
		System.out.println("[1] - Yes");
		System.out.println("[2] - No");

		do { //using loop to validate the user input,  only numbers and just numbers are accepted.
			try {
				symbol = Integer.parseInt(mySc.nextLine());
				if (symbol < 1 || symbol > 2) {
					// not good
					System.out.println("Sorry, your number was not reconigized try it again (number: 1 or 2)");
					valid = false;
				} else {
					valid = true;
				}
			} catch (Exception e) {
				System.out.println("The option must be a number");
				valid = false;
			}
		} while (!valid);

		if (symbol == 1) {
			System.out.println();
			new Matrix();
		} else {
			System.out.println(
					"The system is going to be closed. Thank you, see you later. Your welcome anytime, God bless. Have a great day :) Hasta luego, te veo en River Bar");
			System.exit(1);
		}

	}

	private void processReciprocal() {   //Inverted Matrix
		/* Here we are going to do the calculation for Inverted Matrix, respecting
		 * it's requirements that are: Find the determinant; if the determinant is equal to 0 its not possible
		 * to continue; Use the formula for matrix Inverted.
		 */
		
		System.out.println("You have choosen: Reciprocal (Inverse) Matrix:");

/*		this operation only works with 2x2 matrices. I couldn't improve my code to threat better this step
		 at the moment it has to be both matrices 2x2.*/
		if (numbRowsA > 2 || numbColsA > 2) {
			System.out.println("Sorry, We can not go any further in this operation because the Matrix A has to be 2x2");
			
			processRestartingProgram("Would you like to do another operation?");
		}else if (numbRowsB > 2 || numbColsB > 2){
			System.out.println("Sorry, We can not go any further in this operation because the Matrix B has to be 2x2");
			
			processRestartingProgram("Would you like to do another operation?");
		}else {
			int detA; // determinant Matrix A
			resultDivA = new Double[numbRowsA][numbColsA];
			int detB;// determinant Matrix B
			resultDivB = new Double[numbRowsB][numbColsB];

			//the formula to find the determinant is: ad - bc  , as it's shown below
			detA = ((matrixA[0][0] * matrixA[1][1]) - (matrixA[0][1] * matrixA[1][0]));
		
			resultDivA[0][0] = (double) matrixA[1][1];
			resultDivA[0][1] = (double) -(matrixA[0][1]);
			resultDivA[1][0] = (double) -(matrixA[1][0]);
			resultDivA[1][1] = (double) matrixA[0][0];

			//this is an important step,  the determinant has to be fractioned and multiplicated by every position in the array, that why we use Math.pow
				for (int i = 0; i < numbRowsA; i++) {
					for (int j = 0; j < numbColsA; j++)
						resultDivA[i][j] = (double) (resultDivA[i][j] * Math.pow(detA, -1));
				}

			// same steps above, for the second matrix
			detB = ((matrixB[0][0] * matrixB[1][1]) - (matrixB[0][1] * matrixB[1][0]));

			resultDivB[0][0] = (double) matrixB[1][1];
			resultDivB[0][1] = (double) -(matrixB[0][1]);
			resultDivB[1][0] = (double) -(matrixB[1][0]);
			resultDivB[1][1] = (double) matrixB[0][0];

				for (int i = 0; i < numbRowsB; i++) {
					for (int j = 0; j < numbColsB; j++)
						resultDivB[i][j] = (double) (resultDivB[i][j] * Math.pow(detB, -1));
				}

			System.out.println("\n" + "Result of Matrix A");
				
				//the inverse operation is discarded if the determinant is equal to zero
				if (detA == 0) {
					System.out.println("The determinant is equal to 0, so the Matrix does not have invert.");
					processRestartingProgram("Would you like to do another operation?");
				} else {
					System.out.println("The determinant is: " + detA);
				}

				for (int i = 0; i < resultDivA.length; i++) {
					for (int j = 0; j < resultDivA[i].length; j++) {
						System.out.print(Math.round(resultDivA[i][j] * 1000) / 1000D + "\t");
						//using math.round to round the number to three decimal places
					}
					System.out.println();
				}

			System.out.println("\n" + "@");
			System.out.println("\n" + "Result of Matrix B");

				if (detB == 0) {
					System.out.println("The determinant is equal to 0, so the Matrix does not have invert.");
					processRestartingProgram("Would you like to do another operation?");
				} else {
					System.out.println("The determinant is: " + detB);
				}

				for (int i = 0; i < resultDivB.length; i++) {
					for (int j = 0; j < resultDivB[i].length; j++) {
						System.out.print(Math.round(resultDivB[i][j] * 1000) / 1000D + "\t");
					}
					System.out.println();
				}
				
				saveResults3("Would you like to save the results to a file?" , resultDivA, resultDivB);
				//calling method to save in the file

		}
	}

	private void processDivScalar(int[][] matrix3, int[][] matrix22, String string) {
		// scalar division is defined by a scalar number then it will be used to divide the positions of the matrix 

		resultDivA = new Double[numbRowsA][numbColsA];
		resultDivB = new Double[numbRowsB][numbColsB];
		int scalar = 0;
		System.out.println("You have choosen: Scalar Division");
		System.out.println("\n" + string);

			do {  //using loop to validate the user input,  only numbers and just numbers are accepted.
				try {
					scalar = Integer.parseInt(mySc.nextLine());
					valid = true;
				} catch (Exception e) {
					System.out.println("The numb inputed was not recognized. Please, use any integer number only");
					valid = false;
				}
			}while (!valid);
		
		//this is an important step,  the determinant has to be fractioned and divided by every position in the array, that why we use Math.pow
		for (int i = 0; i < numbRowsA; i++) {
			for (int j = 0; j < numbColsA; j++)
				resultDivA[i][j] = (double) (matrixA[i][j] * Math.pow(scalar, -1));			
		}

		for (int i = 0; i < numbRowsB; i++) {
			for (int j = 0; j < numbColsB; j++)
				resultDivB[i][j] = (double) (matrixB[i][j] * Math.pow(scalar, -1));
		}

		System.out.println("\n" + "Result of Matrix A");

		for (int i = 0; i < resultDivA.length; i++) {
			for (int j = 0; j < resultDivA[i].length; j++) {
				System.out.print((new DecimalFormat("#.##").format(resultDivA[i][j])) + "\t");
				//I used a different decimal format instruction from the reciprocal method because i was learning how to work with these functions and I left like that because it works well.
			}
			System.out.println();
		}

		System.out.println("\n" + "@");
		System.out.println("\n" + "Result of Matrix B");

		for (int i = 0; i < resultDivB.length; i++) {
			for (int j = 0; j < resultDivB[i].length; j++) {
				System.out.print((new DecimalFormat("#.##").format(resultDivB[i][j])) + "\t");
			}
			System.out.println();
		}

		saveResults3("Would you like to save the results to a file?" , resultDivA, resultDivB);
		//calling method to save in the file

	}


	private void processMultMatrices(int[][] matrix3, int[][] matrix22) {
		// Multiplication of matrices is the most boring and complicated method to create.  ;|  I'm joking,    this is very cool operation to create, we just need to increment the loop for the make it multiply row by column of the matrices
		
		resultA = new int[numbRowsA][numbColsA];
				
		System.out.println("You have chosen: Multiply two matrices");
		
		if (numbColsA != numbRowsB) {
			System.out.println("The system cannot continue with this calc because the number of Columns in Matrix A is different of the number of Rows in Matrix B");
			
			processRestartingProgram("Would you like to do another operation?");
		} else {
			
			//Ok, we use a third loop here as a key, to intercalate the position running between rows by columns,  then we can multiply the values
			for (int i = 0; i < matrixA.length; i++) {
				for (int j = 0; j < matrixB[0].length; j++) {
					resultA[i][j] = 0;
					for (int k = 0; k < matrixA[0].length; k++) {
						resultA[i][j] += matrixA[i][k] * matrixB[k][j];
					
						/* System.out.println("i " + i + ", j "+ j +", k " +  k+ ", matrixA " + matrixA[i][k]+ ", matrixB " + matrixB[k][j]+", result " + resultA[i][j]);
						 the print above is just a solution for maths assignment.*/ 
					}
				}
			}
			
			System.out.println("\n" + "The Result Matrix is: ");

			for (int i = 0; i < resultA.length; i++) {
				for (int j = 0; j < resultA[i].length; j++) {
					System.out.print(resultA[i][j] + "\t");
				}
				System.out.println();
			}
			
			saveResults("Would you like to save the results to a file?" , resultA);
			//calling method to save in the file
		}
	}
		
	private void processScalar(int[][] matrix3, int[][] matrix22, String string) {
		// scalar multiplication is the same idea of scalar division the only thing that is changed here is eh Multiply operation :') 
		// it's defined by a scalar number then it will be used to multiply the positions of the matrix 

		resultA = new int[numbRowsA][numbColsA];
		resultB = new int[numbRowsB][numbColsB];
		int scalar = 0;
		
		System.out.println("You have choosen: Scalar Multiplication");
		System.out.println("\n" + string);
		
		do {   // loop for the lovely user don't put anything different of NUMBERS.
			try {
				scalar = Integer.parseInt(mySc.nextLine());
				valid = true;
			} catch (Exception e) {
				System.out.println("The numb inputed was not reconigzed. Please, use any integer number only");
				valid = false;
			}
		}while (!valid);
		

		for (int i = 0; i < numbRowsA; i++) {
			for (int j = 0; j < numbColsA; j++)
				resultA[i][j] = matrixA[i][j] * scalar;
		}
		
		//scalar multiplication for both matrices,  and then the user will be asked if want to save the results.

		for (int i = 0; i < numbRowsB; i++) {
			for (int j = 0; j < numbColsB; j++)
				resultB[i][j] = matrixB[i][j] * scalar;
		}

		System.out.println("\n" + "Result Matrix A");

		for (int i = 0; i < resultA.length; i++) {
			for (int j = 0; j < resultA[i].length; j++) {
				System.out.print(resultA[i][j] + "\t");
			}
			System.out.println();
		}

		System.out.println("\n" + "@");
		System.out.println("\n" + "Result Matrix B");

		for (int i = 0; i < resultB.length; i++) {
			for (int j = 0; j < resultB[i].length; j++) {
				System.out.print(resultB[i][j] + "\t");
			}
			System.out.println();
		}
		
		saveResults2("Would you like to save the results to a file?" , resultA, resultB);
		//calling method to save in the file


	}

	private void processSub(int[][] matrix3, int[][] matrix22) {
		// basic method for subtraction of the matrices
		
		resultA = new int[numbRowsA][numbColsA];

		System.out.println("You have choosen: Subtraction of Two Matrices");

		for (int i = 0; i < numbRowsA; i++) {
			for (int j = 0; j < numbColsA; j++)
				resultA[i][j] = (matrixA[i][j] - matrixB[i][j]);
		}

		System.out.println("\n" + "The Result Matrix is: ");

		for (int i = 0; i < resultA.length; i++) {
			for (int j = 0; j < resultA[i].length; j++) {
				System.out.print(resultA[i][j] + "\t");
			}
			System.out.println();
		}
		
		saveResults("Would you like to save the results to a file?" , resultA);
		//calling method to save in the file

	}

	private void processAdd(int[][] matrix3, int[][] matrix22) {
		// basic method for addition of the matrices
		
		resultA = new int[numbRowsA][numbColsA];

		System.out.println("You have choosen: Addition of Two Matrices");

		for (int i = 0; i < numbRowsA; i++) {
			for (int j = 0; j < numbColsA; j++)
				resultA[i][j] = (matrixA[i][j] + matrixB[i][j]);
		}

		System.out.println("\n" + "The Result Matrix is: ");

		for (int i = 0; i < resultA.length; i++) {
			for (int j = 0; j < resultA[i].length; j++) {
				System.out.print(resultA[i][j] + "\t");
			}
			System.out.println();
		}
		
		saveResults("Would you like to save the results to a file?" , resultA);
		//calling method to save in the file

	}

	


	private void delegUserOper(int symbol) {
		// This is a void method created to delegate the user option to it's operation respectively . 7 option in the total.

		if (symbol == 1) {
			processAdd(matrixA, matrixB);

			processRestartingProgram("Would you like to do another operation?");

		} else if (symbol == 2) {

			processSub(matrixA, matrixB);

			processRestartingProgram("Would you like to do another operation?");

		} else if (symbol == 3) {
			processScalar(matrixA, matrixB, "Please, enter a Scalar Value: (any integer number)");

			processRestartingProgram("Would you like to do another operation?");

		} else if (symbol == 4) {
			processMultMatrices(matrixA, matrixB);

			processRestartingProgram("Would you like to do another operation?");

		} else if (symbol == 5) {
			processDivScalar(matrixA, matrixB, "Please, enter a Scalar Value: (any integer number)");

			processRestartingProgram("Would you like to do another operation?");
		} else if (symbol == 6) {
			processReciprocal();

			processRestartingProgram("Would you like to do another operation?");
		} else {
			System.out.println(
					"The system is going to be closed. Thank you, see you later. Your welcome anytime, God bless. Have a great day :) Hasta luego, te veo en River Bar");

			System.exit(0);
		}
	}

	private int askUserOper(String string) {
		// Simple menu for the operations,  the user must input a number as reference to the operation

		System.out.println();
		System.out.println(string);
		System.out.println("[1] - Add two Matrices");
		System.out.println("[2] - Subtract Matrices");
		System.out.println("[3] - Scalar Multiplication of a Matrix (Multiply a matrix by an Integer)");
		System.out.println("[4] - Multiply two matrices");
		System.out.println("[5] - Scalar Division");
		System.out.println("[6] - Calculate the Reciprocal (or Inverse) of a 2 x 2 Matrix");
		System.out.println("[7] - Quit Program");

		do {  //using loop to validate the user input,  only numbers and just numbers are accepted.
			try {
				symbol = Integer.parseInt(mySc.nextLine());
				if (symbol < 1 || symbol > 7) {
					// not good
					System.out.println("Sorry, your number was not recognized try it again (a number between 1 and 7)");
					valid = false;
				} else {
					valid = true;
				}
			} catch (Exception e) {
				System.out.println("The option must be a number");
				valid = false;
			}
		} while (!valid);
		return symbol;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Matrix();
	}

}
